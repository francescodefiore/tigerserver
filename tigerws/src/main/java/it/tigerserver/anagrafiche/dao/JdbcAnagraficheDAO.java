package it.tigerserver.anagrafiche.dao;

import java.util.List;
import java.util.Map;

import it.tigerserver.jboxlib.JBoxDAO;
import it.tigerserver.anagrafiche.models.ItemMenu;
import it.tigerserver.anagrafiche.models.Negozio;
import it.tigerserver.anagrafiche.models.Utente;

import javax.sql.DataSource;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

public class JdbcAnagraficheDAO implements JBoxDAO{
	private JdbcTemplate jdbcTemplate;
	private String filter_string = "";
	private String filter_columns = "*";
	private String aggregation_string = "";
	private String ordering_string = "";
	
	public void setDataSource(DataSource dataSource) {
		 this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public void print() {
		System.out.println("--- JdbcAnagraficheDAO");
		
	}
	
	

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setName(String name) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addConditionalFilter(String filter_name, String[] filter_params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addAggregationFilter(String filter_name, String[] filter_params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resetFilters() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Map<String, Object>> getData() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	public List<ItemMenu> getItemMenu(int idUtente) {
		List<ItemMenu> itemMenu = null;
		
		try {					
			String sql = "select m.* " +
						"from utenti u " +
						"inner join utenti_gruppi ug " +
						"on u.idutente = ug.idUtente " +
						"inner join gruppi g " +
						"on ug.idGruppo = g.idgruppo " +
						"inner join menu_gruppi mg " +
						"on ug.idGruppo = mg.idGruppo " +
						"inner join menu m " +
						"on mg.idMenu = m.idmenu " +
						"where u.idutente = " + idUtente;

			itemMenu  = this.jdbcTemplate.query(sql,new BeanPropertyRowMapper<ItemMenu>(ItemMenu.class));
					
		}
			
		catch (Exception e) {
			    System.out.println(" Anagrafiche::getItemMenu " + e);
			    
		}	
		
		return itemMenu;
	}
	
	
	public List<Negozio> getNegozio(int idNegozio) {
		List<Negozio> negozio = null;
		
		try {					
			String sql = "select * from negozi where idNegozio= " + idNegozio;

			negozio  = this.jdbcTemplate.query(sql,new BeanPropertyRowMapper<Negozio>(Negozio.class));
					
		}
			
		catch (Exception e) {
			    System.out.println(" Anagrafiche::getNegozio " + e);
			    
		}	
		
		return negozio;
	}
	
	public List<Utente> login(String username)
	{
		List<Utente> utente = null;
		
		try {					
			String sql = "select * from utenti where username ='" + username + "'";
			utente  = this.jdbcTemplate.query(sql,new BeanPropertyRowMapper<Utente>(Utente.class));
			
			if(utente.size()>0)
			{
				utente.get(0).setItemMenu(getItemMenu(utente.get(0).getIdUtente()));
				utente.get(0).setNegozio(getNegozio(utente.get(0).getIdNegozio()));
			}
					
		}
			
		catch (Exception e) {
			    System.out.println(" Anagrafiche::login " + e);
			    
		}	
		
		return utente;
	}
	
	public List<Negozio> getAllNegozi()
	{
		List<Negozio> negozi = null;
		
		try {					
			String sql = "select * from negozi";
			negozi  = this.jdbcTemplate.query(sql,new BeanPropertyRowMapper<Negozio>(Negozio.class));
					
		}
			
		catch (Exception e) {
			    System.out.println(" Anagrafiche::getAllNegozi " + e);
			    
		}	
		
		return negozi;
	}

	

	

}
