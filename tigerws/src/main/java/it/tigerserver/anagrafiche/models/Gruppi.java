package it.tigerserver.anagrafiche.models;

public class Gruppi {

	private int idGruppo;
	private String nome;
	private String descrizione;
	
	public int getIdGruppo() {
		return idGruppo;
	}
	public void setIdGruppo(int idGruppo) {
		this.idGruppo = idGruppo;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDescrizione() {
		return descrizione;
	}
	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}
	
	
}
