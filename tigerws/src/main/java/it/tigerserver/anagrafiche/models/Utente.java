package it.tigerserver.anagrafiche.models;

import java.util.Date;
import java.util.List;

public class Utente {
	
	private int idUtente;
	private String username;
	private String password;
	private String nome;
	private String cognome;
	private Date dataNascita;
	private String indirizzo;
	private String citta;
	private String telefono;
	private String email;
	private int idNegozio;
	private List<ItemMenu> itemMenu;
	private List<Negozio> negozio; 
	
	public int getIdUtente() {
		return idUtente;
	}
	public void setIdUtente(int idUtente) {
		this.idUtente = idUtente;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCognome() {
		return cognome;
	}
	public void setCognome(String cognome) {
		this.cognome = cognome;
	}
	public Date getDataNascita() {
		return dataNascita;
	}
	public void setDataNascita(Date dataNascita) {
		this.dataNascita = dataNascita;
	}
	public String getIndirizzo() {
		return indirizzo;
	}
	public void setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
	}
	public String getCitta() {
		return citta;
	}
	public void setCitta(String citta) {
		this.citta = citta;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getIdNegozio() {
		return idNegozio;
	}
	public void setIdNegozio(int idNegozio) {
		this.idNegozio = idNegozio;
	}
	public List<ItemMenu> getItemMenu() {
		return itemMenu;
	}
	public void setItemMenu(List<ItemMenu> itemMenu) {
		this.itemMenu = itemMenu;
	}
	public List<Negozio> getNegozio() {
		return negozio;
	}
	public void setNegozio(List<Negozio> negozio) {
		this.negozio = negozio;
	}

}
