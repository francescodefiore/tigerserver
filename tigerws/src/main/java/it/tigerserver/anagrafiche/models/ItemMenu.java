package it.tigerserver.anagrafiche.models;

public class ItemMenu {
	
	private int idMenu;
	private String nome;

	public int getIdMenu() {
		return idMenu;
	}

	public void setIdMenu(int idMenu) {
		this.idMenu = idMenu;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

}
