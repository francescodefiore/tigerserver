package it.tigerserver.anagrafiche.models;

public class Negozio {

	private int idNegozio;
	private String denominazione;
	private String indirizzo;
	private String citta;
	private double latitudine;
	private double longitudine;
	
	public int getIdNegozio() {
		return idNegozio;
	}
	public void setIdNegozio(int idNegozio) {
		this.idNegozio = idNegozio;
	}
	public String getDenominazione() {
		return denominazione;
	}
	public void setDenominazione(String denominazione) {
		this.denominazione = denominazione;
	}
	public String getIndirizzo() {
		return indirizzo;
	}
	public void setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
	}
	public String getCitta() {
		return citta;
	}
	public void setCitta(String citta) {
		this.citta = citta;
	}
	public double getLatitudine() {
		return latitudine;
	}
	public void setLatitudine(double latitudine) {
		this.latitudine = latitudine;
	}
	public double getLongitudine() {
		return longitudine;
	}
	public void setLongitudine(double longitudine) {
		this.longitudine = longitudine;
	}
	
	
}
