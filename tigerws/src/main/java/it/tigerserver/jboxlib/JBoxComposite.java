package it.tigerserver.jboxlib;

import java.util.HashMap;
import java.util.Map;


/* 
 * A JBoxComposite is a father node of a tree
 * 
 * [Composite] class of [Composite Pattern]
 */

public class JBoxComposite implements JBox
{	
	private String name;
	private Map<String, JBox> elements = new HashMap<String, JBox>();
	
	public JBoxComposite(String name)
	{
		this.name = name;		
	}
	
	public void add(JBox item)
	{		
		elements.put(item.getName(), item);
	
	}
	
	
	public void remove(JBox item)
	{
		elements.remove(item);
	}
	
	public JBox getChild(int i)
	{
		return (JBox)elements.get(i);
	}

	public String getName() 
	{
		return name;
	}

	public Map<String, JBox> getElements()
	{
		return elements;
	}


	@Override
	public void resetFilters() {
				
		for(Map.Entry<String,JBox> element : elements.entrySet())		
			element.getValue().resetFilters();		
	}
	
	@Override
	public void addConditionalFilter(String filter_name, String[] filter_params) {
		
		for(Map.Entry<String,JBox> element : elements.entrySet())		
			element.getValue().addConditionalFilter(filter_name, filter_params);
		
	}

	@Override
	public void addAggregationFilter(String filter_name, String[] filter_params) {
		for(Map.Entry<String,JBox> element : elements.entrySet())		
			element.getValue().addAggregationFilter(filter_name, filter_params);
		
	}


	@Override
	public void print() {
		System.out.println("Node " + this.getName());
		
		for(Map.Entry<String,JBox> element : elements.entrySet())
		{
			System.out.print(" > child of  " + this.getName() + ": ");
			element.getValue().print();
		}
		
	}

	@Override
	public void setName(String name) {
		this.name = name;
		
	}

	
}