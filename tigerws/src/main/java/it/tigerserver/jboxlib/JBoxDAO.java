package it.tigerserver.jboxlib;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

/* 
 * A JBoxDAO defines a leaf node of the tree
 *     
 * A JBoxDAO defines the methods for accessing data from a database.
 * 
 * [Leaf] class of [Composite Pattern]
 */

public interface JBoxDAO extends JBox
{		
	
	public void setDataSource(DataSource source);
	
	public List< Map<String, Object> > getData();	
}



