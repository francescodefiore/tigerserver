package it.tigerserver.jboxlib;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

public interface JWeather
{		
	
	public void setDataSource(DataSource source);
	
	public List< Map<String, Object> > getData(String city, String dateFrom, String dateTo);
}



