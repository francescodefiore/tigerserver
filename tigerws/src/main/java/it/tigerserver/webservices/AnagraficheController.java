package it.tigerserver.webservices;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import it.tigerserver.anagrafiche.models.ItemMenu;
import it.tigerserver.anagrafiche.models.Negozio;
import it.tigerserver.anagrafiche.models.Utente;
import it.tigerserver.anagrafiche.models.UtenteLogin;
import it.tigerserver.jboxlib.JBox;
import it.tigerserver.jboxlib.JBoxDAO;
import it.tigerserver.anagrafiche.dao.JdbcAnagraficheDAO;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.CrossOrigin;

@Controller
public class AnagraficheController {

	private static final ApplicationContext context =	new ClassPathXmlApplicationContext("Spring-Module.xml");
	
	private static final JBox anagraficheDAO = (JBoxDAO) context.getBean("anagraficheDAO");
	
	public AnagraficheController() {
		System.out.println("==== /AnagraficheController ====");
	} 
	
	
	@RequestMapping(value = "/anagrafica/getitemmenu", method = RequestMethod.GET)
	public @ResponseBody List<ItemMenu> getItemMenu(@RequestParam(value="idUtente", required=true) Integer idUtente) {
		
		return ((JdbcAnagraficheDAO)anagraficheDAO).getItemMenu(idUtente);
	}
	

	@CrossOrigin
	@RequestMapping(value = "/anagrafica/login", method = RequestMethod.POST)
	public @ResponseBody List<Utente> login(@RequestBody UtenteLogin username) {
		
		return ((JdbcAnagraficheDAO)anagraficheDAO).login(username.getUsername());
	}
	
	@CrossOrigin
	@RequestMapping(value = "/anagrafica/getAllNegozi", method = RequestMethod.GET)
	public @ResponseBody List<Negozio> getAllNegozi() {
		
		return ((JdbcAnagraficheDAO)anagraficheDAO).getAllNegozi();
	}
	
	
	
}
