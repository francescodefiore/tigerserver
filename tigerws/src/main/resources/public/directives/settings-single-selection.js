CREEMapp.directive('settingsSingleSelection', function() {

	  return {		
		templateUrl: 'directives/settings-single-selection.html',
				
		controller: ['$scope', 'CreemSettings', function($scope, CreemSettings) {
							
							$scope.selected_cluster = "LAYOUT";
							$scope.setCluster = function() 
							{													
								CreemSettings.setCluster($scope.selected_cluster);
							}
							
							$scope.selected_city = "AG";
							$scope.setCity = function() 
							{													
								CreemSettings.setCity($scope.selected_city);
							}
							
							$scope.setBuilding = function() 
							{													
								CreemSettings.setBuilding($scope.selected_building);
							}
							
							$scope.onClickUpdate = function (tab)
							{
							
								if($scope.currentTab == 'building_tab')
									$scope.setBuilding();								
								if($scope.currentTab == 'cluster_tab')
									$scope.setCluster();									
								if($scope.currentTab == 'city_tab')
									$scope.setCity();
									
							}
							
							
							$scope.building_tab = {	name: 'building_tab'};					
							$scope.cluster_tab = {	name: 'cluster_tab'};
							$scope.city_tab = {	name: 'city_tab'};
							

								$scope.currentTab = 'building_tab';

								$scope.onClickTab = function (tab) {
									$scope.currentTab = tab.name;
									
								}
								
								$scope.isActiveTab = function(name) {
									return name == $scope.currentTab;
								}							
						}
						
		]
		
		
				
				
		
		
	  }
	});
	
	