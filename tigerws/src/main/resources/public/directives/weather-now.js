CREEMapp.directive('weatherNow', 

function() 
{
	
	return {
    controller: ['$scope', 'CreemSettings', '$http', 'WeatherFactory',
		
		function($scope, CreemSettings, $http, WeatherFactory)
		{			
				

			var refreshData = function()
			{
					$scope.city = CreemSettings.selectedbuildings[0].nome;	

					$http.jsonp('http://api.wunderground.com/api/baee12df81a5602b/conditions/q/IT/' + $scope.city + '.json?callback=JSON_CALLBACK').
							success(function(data, status, headers, config) {
							$scope.weatherdata = data;
					})
			}
			
			$scope.$on('settingsUpdated', function (event, data) { refreshData(); });
			
			if(CreemSettings.selectedbuildings.length > 0)
				refreshData();

		}],
		
		templateUrl: 'directives/weather-now.html'
  };
  
});
	
	