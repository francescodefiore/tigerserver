'use strict';

CREEMapp.controller('LoginController', ['$rootScope', '$scope', 'LoginService', '$location', function ($rootScope, $scope, LoginService,$location) {
    if ( LoginService.isLoggedIn() ) {
        $location.path('/dashboard');
    }
	
    $scope.submitLoginForm = function () {
        try {
            LoginService.signIn({
                username: $scope.username,
                password: $scope.password
            });
        }
        catch (exception) {
            console.log(exception);
        }
        finally {}
    }
}]);