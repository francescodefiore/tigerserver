package it.smee.daticampo.dao;

import java.util.List;
import java.util.Map;

import it.smee.buildings.models.Componente;
import it.smee.daticampo.models.DatiPesb;
import it.smee.daticampo.models.EnergyData;
import it.smee.daticampo.models.MisureCampo;
import it.smee.daticampo.models.Schede;
import it.smee.daticampo.models.SensorData;
import it.smee.daticampo.models.WeatherStation;
import it.smee.jboxlib.JBoxDAO;

import javax.sql.DataSource;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

public class JdbcDatiCampoDAO implements JBoxDAO{
	private JdbcTemplate jdbcTemplate;
	private String filter_string = "";
	private String filter_columns = "*";
	private String aggregation_string = "";
	private String ordering_string = "";
	
	public void setDataSource(DataSource dataSource) {
		 this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public void print() {
		System.out.println("--- JdbcDatiCampoDAO");
		
	}
	
	public List<MisureCampo> getDataThermal() {
		List<MisureCampo> daticampo = null;
		
		try {					
			String sql = "SELECT idthermalMeter as idMeasure, o.idOsservazione, val as valore,"+
					     "acquisition_time as giorno, system_id, peso_unita "+
					     "FROM smee_creem.thermalmeter t "+
					     "inner join smee_creem.osservazioni o on t.idOsservazione=o.idOsservazione "+
					     "where stato='OK' "+ filter_string;

			daticampo  = this.jdbcTemplate.query(sql,new BeanPropertyRowMapper<MisureCampo>(MisureCampo.class));
					
		}
			
		catch (Exception e) {
			    System.out.println(" DatiCampoEdificio::Query non eseguita");
			    System.out.println(e);
			    
		}	
		
		return daticampo;
	}
	
	public List<WeatherStation> getDataWeatherStation() {
		List<WeatherStation> daticampo = null;
		
		try {					
			String sql = "SELECT idWeatherStation as idMeasure, o.idOsservazione, windDirection, "+
					     "windVelocity, temperature, humidity, solarRadiation, rainLevel, "+
					     "atmosphericPressure, currentTime as giorno, system_id "+
					     "FROM smee_creem.weather_station t "+
					     "inner join smee_creem.osservazioni o on t.idOsservazione=o.idOsservazione "+
					     "where stato='OK' "+ filter_string;

			daticampo  = this.jdbcTemplate.query(sql,new BeanPropertyRowMapper<WeatherStation>(WeatherStation.class));
					
		}
			
		catch (Exception e) {
			    System.out.println(" DatiCampoEdificio::Query non eseguita");
			    System.out.println(e);
			    
		}	
		
		return daticampo;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setName(String name) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addConditionalFilter(String filter_name, String[] filter_params) {
		
		switch(filter_name)
		{
			case "IMMOBILE":	
				filter_string = filter_string.concat( immobileFilter(filter_params) );				
				break;
			
			case "THERMALINTERVAL":	
				filter_string = filter_string.concat( thermalFilter(filter_params) );				
				break;
				
			case "WEATHERINTERVAL":	
				filter_string = filter_string.concat( weatherFilter(filter_params) );				
				break;
		}
	}

	private String thermalFilter(String[] interval) {
		int i=0;
		String s = " && (acquisition_time BETWEEN '" + interval[i] +"' AND '"+ interval[i+1]+ "'";
		i = i+2;
		while(i<interval.length)
		{
			s = s.concat(" OR acquisition_time BETWEEN '" + interval[i] +"' AND '"+ interval[i+1]+ "'");
			i = i+2;
		}
		s = s.concat(")");
		return s;
	}
	
	private String weatherFilter(String[] interval) {
		int i=0;
		String s = " && (currentTime BETWEEN '" + interval[i] +"' AND '"+ interval[i+1]+ "'";
		i = i+2;
		while(i<interval.length)
		{
			s = s.concat(" OR currentTime BETWEEN '" + interval[i] +"' AND '"+ interval[i+1]+ "'");
			i = i+2;
		}
		s = s.concat(")");
		return s;
	}

	private String immobileFilter(String[] idImmobile) {
		int i=0;
		String s = "&& (idImmobile = '" + idImmobile[i] + "' "; 
		while(++i<idImmobile.length)
			s = s.concat(" OR idImmobile = '" + idImmobile[i] + "' ");
		s = s.concat(")");
		return s;
	}

	@Override
	public void addAggregationFilter(String filter_name, String[] filter_params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resetFilters() {
		filter_string = "";	
	}

	@Override
	public List<Map<String, Object>> getData() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	public List<EnergyData> getEnergyDailyDataFromBuilding(int idedificio)
	{
		List<EnergyData> dati = null;
		
		String sql = "SELECT ROUND(UNIX_TIMESTAMP(O1.timestamp)/(5 * 60)) AS timekey, avg(em.active_power_r + em.active_power_s + em.active_power_t) as valore, ipFisico, O1.timestamp " +
					"FROM osservazioni as O1 join osservazione as O2 on O1.idScheda=O2.idScheda join energyMeterNew as em on O1.idOsservazione = em.idOsservazione " +
					"WHERE DATE(O1.timestamp) = CURDATE() and idEdificio='" + idedificio + "' and O1.stato='OK' and tipoScheda='Energy Meter' group by timekey, ipFisico";
		
		System.out.println("JdbcDatiCampoDAO::getEnergyDailyDataFromBuilding " + sql);
		
		dati = this.jdbcTemplate.query(sql,  new BeanPropertyRowMapper<EnergyData>(EnergyData.class));
		
		return dati;

	}
	
	public List<SensorData> getSensorDailyDataFromBuilding(int idedificio)
	{
		List<SensorData> dati = null;
		
		String sql = "SELECT ROUND(UNIX_TIMESTAMP(O1.timestamp)/(5 * 60)) AS timekey, avg(O1.valore) as valore, O2.nomeFenomeno, O1.timestamp " + 
				     " FROM osservazioni as O1 join osservazione as O2 on O1.idScheda=O2.idScheda " +
				     " WHERE DATE(O1.timestamp) = CURDATE() and idEdificio='"+idedificio+"' and O1.stato='OK' and tipoScheda='Sensor' group by timekey, nomeFenomeno";
		
		System.out.println("JdbcDatiCampoDAO::getEnergyDailyDataFromBuilding " + sql);
		
		dati = this.jdbcTemplate.query(sql,  new BeanPropertyRowMapper<SensorData>(SensorData.class));
		
		return dati;
	}
	
	public List<Schede> getBoardsBuilding(int idEdificio,int tipoScheda)
	{
		List<Schede> schede = null;
		
		String sql = "SELECT @rank:=@rank+1 as 'index', s.ip,ifnull(d.valore,s.ip) ubicazione,s.idTipoScheda scheda, l.ip ips "+
					 "FROM schede s " +
					 "inner join locazioni l " +
					 "on s.idLocazione = l.idLocazione " +
					 "left join dettaglischeda d " +
					 "on s.idScheda = d.idScheda, " +
					 "(SELECT @rank:= 0) r "+
					 "where l.ip like '%.%.%." + idEdificio + "' and s.idTipoScheda = " + tipoScheda;
		
		System.out.println("JdbcDatiCampoDAO::getBoardsBuilding " + sql);
		
		schede = this.jdbcTemplate.query(sql,  new BeanPropertyRowMapper<Schede>(Schede.class));
		
		return schede;
		
	}
	
	public List<DatiPesb> getDatiPesb(String ips,String ipf, String da, String a)
	{
		List<DatiPesb> datiPesb = null;
		
		if(ips != null && ipf != null)
		{
			String sql = "SELECT ifnull(sh.valore,0) valore,sh.timestamp data " +
						  "FROM schede s " + 
						  "inner join locazioni l " + 
						  "on s.idLocazione = l.idLocazione "+ 
						  "inner join smee_storicoosservazioni_hour sh " +
						  "on s.idScheda = sh.idScheda " +
						  "where s.idTipoScheda = 7 and l.ip = '" + ips +"' and s.ip = '" + ipf +"' " +
						  "and sh.timestamp >= '" + da + "' and sh.timestamp < date_add( '" + a + "',interval 1 day) " +
						  "order by sh.timestamp asc";
			
			System.out.println("JdbcDatiCampoDAO::getDatiPesb " + sql);
			
			datiPesb = this.jdbcTemplate.query(sql,  new BeanPropertyRowMapper<DatiPesb>(DatiPesb.class));
		}
		
		return datiPesb;
		
	}
	
	public List<DatiPesb> getDatiEnergy(String ipl,String ipe,String colonna, String da, String a)
	{
		List<DatiPesb> datiPesb = null;
		
		if(ipl != null && ipe != null && colonna != null)
		{
			String sql = "SELECT ifnull(e." + colonna + " ,0) valore,e.acquisition_time data " +
						  "FROM schede s " + 
						  "inner join locazioni l " + 
						  "on s.idLocazione = l.idLocazione "+ 
						  "inner join smee_energymeternew_hour e " +
						  "on s.ip = concat('0.',e.system_id) " +
						  "where l.ip = '" + ipl +"' and e.system_id  = '" + ipe +"' " +
						  "and e.acquisition_time >= '" + da + "' and e.acquisition_time < date_add('" + a + "',interval 1 day) " +
						  "order by e.acquisition_time asc";
			
			System.out.println("JdbcDatiCampoDAO::getDatiEnergy " + sql);
			
			datiPesb = this.jdbcTemplate.query(sql,  new BeanPropertyRowMapper<DatiPesb>(DatiPesb.class));
		}
		
		return datiPesb;
		
	}
	
	public List<DatiPesb> getDatiThermal(String iptl,String ipt,String colonna, String da, String a)
	{
		List<DatiPesb> datiPesb = null;
		
		if(iptl != null && ipt != null && colonna != null)
		{
		
			String sql =  "SELECT ifnull(valore,0) valore, data " +
						  "FROM( " +
						  "SELECT t." + colonna + " - @quot valore,@quot:= t.val curr_value, t.acquisition_time data " +
						  //"SELECT t." + colonna + " valore,t.acquisition_time data " +
						  "FROM schede s " + 
						  "inner join locazioni l " + 
						  "on s.idLocazione = l.idLocazione "+ 
						  "inner join smee_thermalmeter_hour t " +
						  "on s.ip = concat('0.',t.system_id) " +
						  ",(select @quot:=0)q " +
						  "where l.ip = '" + iptl +"' and t.system_id  = '" + ipt +"' " +
						  "and t.acquisition_time >= '" + da + "' and t.acquisition_time < date_add('" + a + "',interval 1 day) " +
						  "order by t.acquisition_time asc " +
						  ")val " + 
						  "LIMIT 18446744073709551615 OFFSET 1";
			
			System.out.println("JdbcDatiCampoDAO::getDatiThermal " + sql);
			
			datiPesb = this.jdbcTemplate.query(sql,  new BeanPropertyRowMapper<DatiPesb>(DatiPesb.class));
			
			
		}
		
		return datiPesb;
		
	}

	

}
