package it.smee.daticampo.models;

public class SensorData
{
	private String timestamp;
	private String nomeFenomeno;
	private float valore;
	
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	public String getNomeFenomeno() {
		return nomeFenomeno;
	}
	public void setNomeFenomeno(String nomeFenomeno) {
		this.nomeFenomeno = nomeFenomeno;
	}
	public float getValore() {
		return valore;
	}
	public void setValore(float valore) {
		this.valore = valore;
	}	
	
}