package it.smee.webservices;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@ComponentScan
@EnableAutoConfiguration
@EnableConfigurationProperties
public class App{

    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }
}
