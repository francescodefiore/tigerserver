'use strict';

CREEMapp.controller('DSSMapController', ['$rootScope', '$scope', 'leafletData', 'BuildingsFactory', 'ManutenzioneFactory', 'CreemSettings',
		function ($rootScope, $scope, leafletData, BuildingsFactory, ManutenzioneFactory, CreemSettings) 
		{

			$scope.center = {};			
			$scope.immobili = [];
			$scope.buildings = {};
			$scope.markers = [];						
			
			
			$scope.layers = {
									baselayers: {
										osm: {
											name: 'OpenStreetMap',
											type: 'xyz',
											url: 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'
										   }
									},
									overlays: 
									{	
										Interventi: { name: "Interventi", type:"markercluster", visible: true } 
									
									}						
									
									
								};



			var refreshData = function ()
			{				

				$scope.buildings = {};								
				
				$scope.markers = [];
				
				$scope.immobili = BuildingsFactory.getBuildings().query(function(){
							
							var len = $scope.immobili.length;
							
								
						
							if(CreemSettings.selectedbuildings.length > 0)
								$scope.buildings[CreemSettings.selectedbuildings[0].codice] = CreemSettings.selectedbuildings[0];
								
							
							if(CreemSettings.selectedClusters.length > 0)														
								for (var i=0; i<len; i++) 
								{									
									if($scope.immobili[i].clusterEnergy == CreemSettings.selectedClusters[0])
										$scope.buildings[$scope.immobili[i].codice] = $scope.immobili[i];
								}
								
								
								
							if(CreemSettings.selectedCities.length > 0)														
								for (var i=0; i<len; i++) 
								{									
									if($scope.immobili[i].siglaprov == CreemSettings.selectedCities[0])
										$scope.buildings[$scope.immobili[i].codice] = $scope.immobili[i];
								}



								$scope.center = {
									lat: 37.4280017,
									lng: 13.7893994,
									zoom: 8
								};
								
								console.log($scope.buildings);
								
								var data =  ManutenzioneFactory.getALLBuildingsInterventi().query(
										function() 
										{	
											var awesomeMarkerIcon = {
																type: 'awesomeMarker',
																icon: 'exclamation-sign',
																prefix: 'glyphicon',
																markerColor: 'orange'
																
															};
															
											for(var i=0; i<data.length; i++) {						
												data[i]["idData"] = i;
												if (data[i]["tipologiaIntervento"]==10)
													data[i]["tipologiaIntervento"] = "Sostituzione";
													
													data[i]["statoAttuale"] = "Modello: " + data[i].modVecchioComp +
																			  "<br> Prestazione: " + data[i].prestazioniVecchioComp +
																			  "<br> Num. Cambio: " + data[i].numCompDaCambiare +
																			  "<br> Consumo Totale(W): " + data[i].consumoTotOld + 
																			  "<br> Costo (€): " + data[i].costoTotOld ;
																
													data[i]["statoProposto"] = "Modello: " + data[i].modNuovoComp + 
																			   "<br> Prestazione: " + data[i].prestazioniNuovoComp + 
																			   "<br> Num. Cambio: " + data[i].numComponentiDaComprare + 
																			   "<br> Potenza (W): " + data[i].potenza + 
																			   "<br> Valore Economico (€): " + data[i].valSingoloComp +  
																			   "<br> Consumo Tot. (W): " + data[i].consumoTotComponenti +  
																			   "<br> Costo Tot. (€): " + data[i].costoComponenti ;
																   
													try{
														$scope.markers.push({
																			layer: "Interventi",
																			lat: $scope.buildings[data[i].codice].latitudine,
																			lng: $scope.buildings[data[i].codice].longitudine,
																			message: "<b>" + data[i]["codice"] + "</b> " + $scope.buildings[data[i]["codice"]].indirizzo + " (" + $scope.buildings[data[i]["codice"]].siglaprov +") <br><b>Intervento " + data[i]["tipologiaIntervento"] + "</b><br><br> <b>Componente attuale<br></b> " + data[i]["statoAttuale"] + "<br><br> <b>Componente proposto<br></b> " + data[i]["statoProposto"],
																			focus: false,
																			icon: awesomeMarkerIcon,											
																			draggable: false											
																});
													}catch(err){}
													
												
											}	

																							

										}
									);
				
								
								
/*
								var alert =  AlertFactory.getALLBuildingAlert().query(
											function() 
											{	
							
												
												for(var i=0; i<alert.length; i++)
												{														
														
														if($scope.buildings[alert[i]["codice"]] != undefined)																												
														{
														
															   alert[i]["super"] = parseFloat(((parseFloat(alert[i]["KPI"]).toFixed(2) - parseFloat(alert[i]["Bench"]).toFixed(2))/parseFloat(alert[i]["Bench"]).toFixed(2))*100).toFixed(2);
															
																							
															
															var awesomeMarkerIcon = {
																type: 'awesomeMarker',
																icon: 'exclamation-sign',
																prefix: 'glyphicon',
																markerColor: 'orange'
																
															};
															if(alert[i]["super"] > 100.0)
																awesomeMarkerIcon.markerColor = "red";
															else
																awesomeMarkerIcon.markerColor = "orange";
															
															$scope.markers.push({
																		layer: "alert",
																		lat: $scope.buildings[alert[i]["codice"]].latitudine,
																		lng: $scope.buildings[alert[i]["codice"]].longitudine,
																		message: "<b>" + alert[i]["codice"] + "</b> " + $scope.buildings[alert[i]["codice"]].indirizzo + " (" + $scope.buildings[alert[i]["codice"]].siglaprov +") <br><b>KPI:</b> " + alert[i]["descrizione"] + "<br> <b>Periodo:</b> " + alert[i]["periodoRiferimento"] + "<br> <b>Superamento benchmark:</b> " + alert[i]["super"] + "%",
																		focus: false,
																		icon: awesomeMarkerIcon,											
																		draggable: false											
															});
														}
												
												}																							

											});
											*/
								
								
																
								
								
																						

				 });
				
			}
			
			
			$scope.$on('settingsUpdated', function (event, data) { refreshData(); } );
			
					
			if(CreemSettings.selectedbuildings.length > 0)
					refreshData();

				
		}
]);