
CREEMapp.factory('LoadingFactory', ['$rootScope', 

	function($rootScope) 
	{
			var LoadingFactory = {};

			
			LoadingFactory.startLoading = function() 
			{				
				$rootScope.isViewLoading = true;
			}
			
			LoadingFactory.stopLoading = function() 
			{
				$rootScope.isViewLoading = false;
			}
			
			
			
			return LoadingFactory;
	}
]);


